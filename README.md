# procon

a extremly simple terminal pro/con list for getting a fast overview

## concept
procon generates a json data-structure in /tmp that will be edited to keep state. Lists can be saved to other location by save command if it is necessary.  

## usage
    procon new <listsname> -> create a new pro/con-list
    procon **+** easy to use  -> add a **pro** argument for current list
    procon **-** hard to use  -> add a **contra** argument for current list
    procon save <path> -> save the list to make it recoverable after reboot
    procon delete -> delete the current list

## list-json
{
"name": "beispiel",
"pro":[["geht schnell", 0], ["sieht gut aus",0]],
"contra:[["teuer", 0]]
}

## todos
[ ] parsing a bit akward because cmd is very broad (check if there are other python-libs that allow non-'-'-starting positional arguments
[ ] add weights to arguments
