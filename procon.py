#!/usr/bin/python

from enum import Enum
import argparse
import json
import os
import shutil

defaultPath = "/tmp/procon_default"


class CMD(Enum):
    NEW = 1
    ADD_POSITIVE = 2
    ADD_NEGATIVE = 3
    SAVE = 4
    DELETE = 5


class List:
    def __init__(self, name):
        self.name = name
        self.pro = []
        self.contra = []

    def to_json(self):
        return json.dumps(self, default=lambda o: o.__dict__,
                          sort_keys=True, indent=4)

    def fill_from_json(self, json_dict):
        self.name = json_dict["name"]
        self.pro = json_dict["pro"]
        self.contra = json_dict["contra"]


def main():
    cmd, arguments = parse_args()
    if cmd == CMD.NEW:
        create_new_list(arguments)
    elif cmd == CMD.ADD_POSITIVE:
        add_point(arguments, True)
    elif cmd == CMD.ADD_NEGATIVE:
        add_point(arguments, False)
    elif cmd == CMD.SAVE:
        save_current_list(arguments)
    elif cmd == CMD.DELETE:
        delete_current_list(arguments)
    elif cmd == CMD.LOAD:
        load_list(arguments)
    else:
        print_usage()
    return 0


def create_new_list(arguments):
    """
    creates a list with the name given in arguments and puts a copy of it to the default-path in /tmp
    :param arguments:
    :return:
    """
    # getting all strings after keyword from cli
    list_name = arguments.cmd[1:]
    list_name = " ".join(list_name)
    if arguments.d:
        print(f'name of the list: {list_name}')
    new_list = List(list_name)
    write_list_to_fs(arguments, new_list)
    print(f'created list {list_name}')


def load_list(args):
    """
    loads a procon-list json from the argument given after save
    overwrites existing list at /tmp/<default_path>
    :param args:
    :return:
    """
    path = args.cmd[1:2]
    list_to_save = read_list_from_fs(args, path)
    if args.d:
        print(f'found list: {list_to_save}')
    write_list_to_fs(args, list_to_save)


def delete_current_list(args):
    """
    removes list that is currently at the default-path
    """
    if args.d:
        print("deleting current list")
    os.remove(defaultPath)


def read_list_from_fs(args, path=defaultPath):
    with open(path, 'r') as f:
        json_list = json.loads(f.read())
        if args.d:
            print(f'json read: {json_list}')
    cur_list = List("")
    cur_list.fill_from_json(json_list)
    return cur_list


def write_list_to_fs(arguments, new_list):
    list_json = new_list.to_json()
    if arguments.d:
        print(f'json is looking like this: {list_json}')
    # write json to fs
    with open(defaultPath, mode='w') as f:
        f.write(list_json)


def add_point(args, is_pro):
    try:
        cur_list = read_list_from_fs(args)
    except FileNotFoundError:
        cur_list = List("default")
        write_list_to_fs(args, cur_list)
    argument = args.cmd[2:]
    if is_pro:
        cur_list.pro.append(argument)
    else:
        cur_list.contra.append(argument)
    write_list_to_fs(args, cur_list)
    print_list(cur_list)


def print_list(cur_list):
    length_list_name = len(cur_list.name)
    seperators = '-' * length_list_name + '\n'
    pros = ' (+)\n'
    cons = ' (-)\n'
    for elem in cur_list.pro:
        pros = pros + f'* {str(elem[0])}\n'
    for elem in cur_list.contra:
        cons = cons + f'* {str(elem[0])}\n'
    print(f'{cur_list.name}\n{seperators}{pros}{cons}')


def save_current_list(args):
    path_for_moving = args.cmd[1:2][0]
    if args.d:
        print(f'moving file from {defaultPath} to {path_for_moving}')
    shutil.move(defaultPath, f'{path_for_moving}/{read_list_from_fs(args).name}')


def print_usage():
    pass


def parse_args():
    """
    Parses the user input from terminal
    returns the command found as a CMD-Enum
    """
    parser = argparse.ArgumentParser("a simple terminal pro/con-list")
    parser.add_argument('cmd', nargs='+', action='store', default=None)
    parser.add_argument('-d', action='store_true', default=False)
    args = parser.parse_args()
    if args.d:
        print(f'namespace created: {args}')
    if args.cmd[0] == "new":
        return CMD.NEW, args
    elif args.cmd[0] == "save":
        return CMD.SAVE, args
    elif args.cmd[0] == "add" and args.cmd[1] == '+':
        return CMD.ADD_POSITIVE, args
    elif args.cmd[0] == "add" and args.cmd[1] == '-':
        return CMD.ADD_NEGATIVE, args
    elif args.cmd[0] == "delete":
        return CMD.DELETE, args
    return None, None


if __name__ == '__main__':
    main()